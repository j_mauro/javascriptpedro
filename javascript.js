describe("sum function",function(){
	it("should sum two numbers", function() {
		expect(sum(1,2)).toBe(3);
		expect(sum(1,4)).toBe(5);
		expect(sum(800,2400)).toBe(3200);
		expect(sum(52.1,34.6)).toBe(86.7);
	});

	it("should sum all numbers passed", function() {
		expect(sum(1,2,3,5,8,5)).toBe(24);
		expect(sum(1,2,3)).toBe(6);	
		expect(sum(1,2,3,5,8,5,19)).toBe(43);
	});

});

function sum(){
	var result = 0;
	for (var i = 0 ; i < arguments.length; i++) {
	 	result = result + arguments[i];
	 }; 
	 return result
}


